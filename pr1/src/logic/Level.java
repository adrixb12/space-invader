package logic;

public enum Level {
	EASY(4,2,0.1,3,0.5), HARD(8,2,0.3,2,0.2), INSANE(8,4,0.5,1,0.1);

	private int numNavesComunes, numNavesDestructoras, velocidad;
	private double frecDisparo, ovni;
	
	
	private Level(int numNavesComunes, int numNavesDestructoras, double frecDisparo, int velocidad, double ovni) {
		this.numNavesComunes = numNavesComunes;
		this.numNavesDestructoras = numNavesDestructoras;
		this.velocidad = velocidad;
		this.frecDisparo = frecDisparo;
		this.ovni = ovni;
	}


	public int getNumNavesComunes() {
		return numNavesComunes;
	}


	public int getNumNavesDestructoras() {
		return numNavesDestructoras;
	}


	public int getVelocidad() {
		return velocidad;
	}


	public double getFrecDisparo() {
		return frecDisparo;
	}


	public double getOvni() {
		return ovni;
	}
	
	
	
	
	
	

}
