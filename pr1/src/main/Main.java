package main;

import java.util.Random;
import java.util.Scanner;

import control.Controller;
import logic.Game;
import logic.Level;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		long seed;
		
		if (args.length == 2) {
			seed = Long.parseLong(args[1]);
		}else {
			seed = new Random().nextInt(1000);   
		}
		Level level = Level.valueOf(args[0].toString());
		
		Random rand = new Random(seed);

		Game game = new Game(level, rand);
		
		Controller controller = new Controller(game, in);
		
		controller.run();

	}

}
