package control;

import java.util.Scanner;
import logic.Game;

public class Controller {
	private Game game;
	private Scanner in;
	private boolean finished = false;
	private String comando;
	private String[] subcomandos = new String[2];
	
	public Controller(Game game, Scanner in) {
		this.game = game;
		this.in = in;
	}
	
	public void run() {
		while (!finished) {
			System.out.print("Comando > ");
			comando = in.nextLine();
			comando = comando.trim();
			comando = comando.toUpperCase();
			subcomandos = comando.split(" ");
			
			if(subcomandos.length == 1) {
				
				if(subcomandos[0].length() > 1 && !subcomandos[0].equals("SHOCKWAVE")) {
					if(subcomandos[0].equals("SHOOT") || subcomandos[0].equals("RESET") 
							|| subcomandos[0].equals("LIST") || subcomandos[0].equals("EXIT") 
							|| subcomandos[0].equals("HELP") || subcomandos[0].equals("NONE")) {
						subcomandos[0] = subcomandos[0].substring(0, 1);
					}
				} else if (subcomandos[0].equals("SHOCKWAVE")) {
					subcomandos[0] = "W";
				}
				
				switch (subcomandos[0]) {
				case "S":
					break;
					
				case "W":
					break;
					
				case "R":
					break;
					
				case "L":
					break;
					
				case "E":
					break;
					
				case "H":
					System.out.println("move <left|right><1|2>: Moves UCM-Ship to the indicated direction.\r\n" + 
							"shoot: UCM-Ship launches a missile.\r\n" + 
							"shockWave: UCM-Ship releases a shock wave.\r\n" + 
							"list: Prints the list of available ships.\r\n" + 
							"reset: Starts a new game.\r\n" + 
							"help: Prints this help message.\r\n" + 
							"exit: Terminates the program.\r\n" + 
							"[none]: Skips one cycle.\r\n" + 
							"");
					break;
					
				case "N":
					break;
					
				default:
					System.out.println("Comando no v�lido\r\n");
					break;
				} 
			}
			
		}
	}
	
	
}
